/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   actions.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/20 17:05:41 by thzeribi          #+#    #+#             */
/*   Updated: 2022/03/25 05:22:30 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ACTIONS_H
# define ACTIONS_H

# define A_THINK "is thinking\n"
# define A_SLEEP "is sleeping\n"
# define A_FORK "has taken a fork\n"
# define A_EATING "is eating\n"

# define A_THINK_E "is thinking         💭 |\n"
# define A_SLEEP_E "is sleeping         😴 |\n"
# define A_R_FORK_E "took the right fork 🍴 |\n"
# define A_L_FORK_E "took the left fork  🍴 |\n"
# define A_FORK_E "take the forks      🍴 |\n"
# define A_EATING_E "is eating           🍜 |\n"

#endif