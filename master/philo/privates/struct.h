/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   struct.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/20 17:05:30 by thzeribi          #+#    #+#             */
/*   Updated: 2022/04/02 02:08:49 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRUCT_H
# define STRUCT_H

# include <pthread.h>

typedef struct s_data		t_data;
typedef struct s_socrate	t_socrate;
typedef struct s_info		t_info;

struct s_socrate {
	unsigned int	id;
	long long int	last_meal;
	int				is_eating;
	int				eat_count;
	int				philo_nbr;
	int				time2eat;
	int				time2die;
	int				time2sleep;
	int				must_eat;
	int				l_fork;
	int				r_fork;
	t_data			*data;
	pthread_t		thread;
	pthread_mutex_t	script;
	pthread_mutex_t	dress;
};

struct s_data {
	int				philo_nbr;
	int				time2die;
	int				time2eat;
	int				time2sleep;
	int				must_eat;
	long long		start_time;
	int				dug_grave;
	int				is_finish;
	int				sat_soc;
	t_socrate		*socrate;
	pthread_mutex_t	*forks;
	pthread_mutex_t	desk;
};

struct s_info {
	unsigned int	id;
	long long int	last_meal;
	int				is_eating;
};

#endif