/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/17 06:09:58 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/21 09:50:48 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void
	display_time(long long start_time)
{
	int			len_time;
	long long	time;

	time = get_current_time() - start_time;
	len_time = len_nb(time);
	if (EMOJI)
	{
		printf("| ");
		while (len_time < 5)
		{
			printf(".");
			len_time++;
		}
		printf("%lldms", time);
	}
	else
		printf("%lld\t", time);
}

void
	display_id(long long id)
{
	int	len;

	len = len_nb(id);
	if (EMOJI)
		printf("    %lld%*c", id, (9 - len), ' ');
	else
		printf("%lld\t", id);
}
