/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manager.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/17 06:05:50 by thzeribi          #+#    #+#             */
/*   Updated: 2022/04/03 23:35:55 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

static void
	eat(t_data *data, t_socrate *socrate)
{
	const int	id = socrate->id;

	if (EMOJI)
		display(data, A_FORK_E, id);
	else
		display(data, A_FORK, id);
	pthread_mutex_lock(&(socrate->dress));
	socrate->is_eating = TRUE;
	socrate->last_meal = get_current_time();
	pthread_mutex_unlock(&(socrate->dress));
	if (EMOJI)
		display(data, A_EATING_E, id);
	else
		display(data, A_EATING, id);
	pthread_mutex_unlock(&(socrate->script));
	ft_usleep(data, socrate->time2eat);
	pthread_mutex_lock(&(socrate->dress));
	socrate->eat_count++;
	socrate->is_eating = FALSE;
	pthread_mutex_unlock(&(socrate->dress));
	pthread_mutex_lock(&(socrate->script));
}

static void
	a_eat(t_data *data, t_socrate *socrate)
{
	pthread_mutex_lock(&(socrate->script));
	pthread_mutex_lock(&(data->forks[socrate->l_fork]));
	pthread_mutex_lock(&(data->forks[socrate->r_fork]));
	eat(data, socrate);
	pthread_mutex_unlock(&(data->forks[socrate->l_fork]));
	pthread_mutex_unlock(&(data->forks[socrate->r_fork]));
	if (socrate->must_eat > 0 && socrate->eat_count == socrate->must_eat)
		incr_satisfied_socrate(data);
	pthread_mutex_unlock(&(socrate->script));
}

static void
	a_sleep(t_data *data, int id)
{
	int const	time2sleep = data->time2sleep;

	if (EMOJI)
		display(data, A_SLEEP_E, id);
	else
		display(data, A_SLEEP, id);
	ft_usleep(data, time2sleep);
}

static void
	a_think(t_data *data, int id)
{
	if (EMOJI)
		display(data, A_THINK_E, id);
	else
		display(data, A_THINK, id);
}

void
	action_manager(t_data *data, t_socrate *socrate, int id)
{
	a_eat(data, socrate);
	a_sleep(data, id);
	a_think(data, id);
}
