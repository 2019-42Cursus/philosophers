/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philo.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/03 05:51:00 by thzeribi          #+#    #+#             */
/*   Updated: 2022/04/03 23:51:41 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

static int
	start_philo(t_data *data)
{
	if (!init_socrate(data) || !mutex_init(data))
	{
		free(data->socrate);
		return (FALSE);
	}
	if (EMOJI)
		display_visual();
	if (!create_socrate_thread(data))
	{
		exit_philo(data);
		return (FALSE);
	}
	if (!data->dug_grave)
		display_end(data);
	exit_philo(data);
	return (TRUE);
}

static int
	parse_arg(char *argv)
{
	if (!ft_isdigit(argv))
		return (-1);
	return (ft_atoi(argv));
}

static int
	init_philo(t_data *data, char *argv[], int argc)
{
	data->philo_nbr = parse_arg(argv[1]);
	data->time2die = parse_arg(argv[2]);
	data->time2eat = parse_arg(argv[3]);
	data->time2sleep = parse_arg(argv[4]);
	if (argc == 6)
		data->must_eat = parse_arg(argv[5]);
	if (data->philo_nbr <= 0 || data->time2die == -1
		|| data->time2eat == -1 || data->time2sleep == -1
		|| data->must_eat == -1)
		return (FALSE);
	return (TRUE);
}

int
	main(int argc, char *argv[])
{
	t_data	data;

	data = (t_data){.must_eat = -2};
	if (argc < 5 || argc > 6)
	{
		printf(RED "Invalid number of arguments !\n" RESET);
		return (1);
	}
	if (!init_philo(&data, argv, argc))
	{
		printf(RED "Invalid values in arguments !\n" RESET);
		return (1);
	}
	if (!start_philo(&data))
	{
		printf(RED "Error during program execution !\n" RESET);
		return (1);
	}
	return (0);
}
