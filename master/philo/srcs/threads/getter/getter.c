/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   getter.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/17 09:50:03 by thzeribi          #+#    #+#             */
/*   Updated: 2022/03/17 03:56:20 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

int
	is_finish(t_data *data)
{
	int	is_finish;

	pthread_mutex_lock(&(data->desk));
	is_finish = data->is_finish;
	pthread_mutex_unlock(&(data->desk));
	return (is_finish);
}

int
	is_eating(t_socrate *socrate)
{
	int	is_eating;

	pthread_mutex_lock(&(socrate->script));
	is_eating = socrate->is_eating;
	pthread_mutex_unlock(&(socrate->script));
	return (is_eating);
}

int
	get_sat_soc(t_data *data)
{
	int	sat_soc;

	pthread_mutex_lock(&(data->desk));
	sat_soc = data->sat_soc;
	pthread_mutex_unlock(&(data->desk));
	return (sat_soc);
}
