/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grim_reaper.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/17 06:51:00 by thzeribi          #+#    #+#             */
/*   Updated: 2022/04/01 23:17:16 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

static void
	kill_socrate(t_data *data, int id)
{
	if (!data->is_finish)
	{
		pthread_mutex_lock(&(data->desk));
		pthread_mutex_unlock(&(data->forks[data->socrate[id - 1].l_fork]));
		pthread_mutex_unlock(&(data->desk));
		set_is_finish(data, TRUE);
		display_death(data, id);
	}
}

void
	grim_reaper(t_data *data)
{
	int			i;
	int const	philo_nbr = data->philo_nbr;
	int const	time2die = data->time2die;
	t_info		c_socrate;

	i = 0;
	while (!is_finish(data))
	{
		pthread_mutex_lock(&(data->socrate[i].dress));
		c_socrate = *((t_info *)(data->socrate + i));
		pthread_mutex_unlock(&(data->socrate[i].dress));
		if (get_current_time() > c_socrate.last_meal + time2die
			&& !c_socrate.is_eating)
			kill_socrate(data, i + 1);
		if (++i >= philo_nbr)
		{
			i = 0;
			usleep(1000);
		}
	}
}
