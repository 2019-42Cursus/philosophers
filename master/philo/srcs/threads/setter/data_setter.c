/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data_setter.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/17 10:16:13 by thzeribi          #+#    #+#             */
/*   Updated: 2022/03/25 05:21:17 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void
	set_is_finish(t_data *data, int boolean)
{
	pthread_mutex_lock(&(data->desk));
	data->is_finish = boolean;
	pthread_mutex_unlock(&(data->desk));
}

void
	set_dug_grave(t_data *data, int boolean)
{
	pthread_mutex_lock(&(data->desk));
	data->dug_grave = boolean;
	pthread_mutex_unlock(&(data->desk));
}

void
	incr_satisfied_socrate(t_data *data)
{
	pthread_mutex_lock(&(data->desk));
	data->sat_soc++;
	if (data->sat_soc >= data->philo_nbr)
		data->is_finish = TRUE;
	pthread_mutex_unlock(&(data->desk));
}
