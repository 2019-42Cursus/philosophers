/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   socrate_setter.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/17 10:28:07 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/18 06:09:24 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void
	incr_meal(t_socrate *socrate)
{
	pthread_mutex_lock(&(socrate->script));
	socrate->eat_count++;
	pthread_mutex_unlock(&(socrate->script));
}
