/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   socrate.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/10 00:13:53 by thzeribi          #+#    #+#             */
/*   Updated: 2022/03/27 04:07:02 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

int
	mutex_init(t_data *data)
{
	int	i;

	i = -1;
	data->forks = malloc(sizeof(pthread_mutex_t) * data->philo_nbr);
	if (data->forks == NULL)
		return (FALSE);
	while (++i < data->philo_nbr)
	{
		if (pthread_mutex_init(&(data->forks[i]), NULL))
			return (FALSE);
		if (pthread_mutex_init(&(data->socrate[i].script), NULL))
			return (FALSE);
		if (pthread_mutex_init(&(data->socrate[i].dress), NULL))
			return (FALSE);
	}
	if (pthread_mutex_init(&(data->desk), NULL))
		return (FALSE);
	return (TRUE);
}

int
	init_socrate(t_data *data)
{
	int	i;

	i = -1;
	data->socrate = malloc(sizeof(t_socrate) * data->philo_nbr);
	if (data->socrate == NULL)
		return (FALSE);
	data->start_time = get_current_time();
	while (++i < data->philo_nbr)
	{
		data->socrate[i].id = i + 1;
		data->socrate[i].eat_count = 0;
		data->socrate[i].last_meal = data->start_time;
		data->socrate[i].data = data;
		data->socrate[i].is_eating = FALSE;
		data->socrate[i].l_fork = i;
		data->socrate[i].r_fork = (i + 1) % data->philo_nbr;
		data->socrate[i].philo_nbr = data->philo_nbr;
		data->socrate[i].time2eat = data->time2eat;
		data->socrate[i].time2die = data->time2die;
		data->socrate[i].time2sleep = data->time2sleep;
		data->socrate[i].must_eat = data->must_eat;
	}
	return (TRUE);
}

int
	create_socrate_thread(t_data *data)
{
	int		i;

	i = -1;
	pthread_mutex_lock(&(data->desk));
	while (++i < data->philo_nbr)
	{
		if (pthread_create(&(data->socrate[i].thread), NULL, &supervisor,
				&data->socrate[i]))
			return (FALSE);
	}
	pthread_mutex_unlock(&(data->desk));
	grim_reaper(data);
	return (TRUE);
}
