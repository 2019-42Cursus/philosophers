/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   supervisor.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/19 13:26:28 by thzeribi          #+#    #+#             */
/*   Updated: 2022/03/25 05:21:04 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void
	*supervisor(void *d)
{
	t_socrate *const	socrate = (t_socrate *) d;
	t_data *const		data = socrate->data;
	int const			id = socrate->id;
	int const			philo_nbr = data->philo_nbr;

	pthread_mutex_lock(&(data->desk));
	pthread_mutex_unlock(&(data->desk));
	if (id % 2 == 0)
		usleep((philo_nbr * 50) + 100);
	while (!is_finish(data))
	{
		action_manager(data, socrate, id);
	}
	return (NULL);
}
