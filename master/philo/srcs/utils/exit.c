/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/15 02:46:49 by thzeribi          #+#    #+#             */
/*   Updated: 2022/04/02 02:08:47 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void
	exit_philo(t_data *data)
{
	int	i;

	i = -1;
	while (++i < data->philo_nbr)
		pthread_join(data->socrate[i].thread, NULL);
	i = 0;
	while (++i < data->philo_nbr)
	{
		pthread_mutex_destroy(&(data->forks[i]));
		pthread_mutex_destroy(&(data->socrate[i].script));
		pthread_mutex_destroy(&(data->socrate[i].dress));
	}
	free(data->socrate);
	free(data->forks);
	pthread_mutex_destroy(&(data->desk));
}
