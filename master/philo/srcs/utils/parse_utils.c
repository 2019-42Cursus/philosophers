/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/08 00:13:01 by thzeribi          #+#    #+#             */
/*   Updated: 2022/02/10 00:03:44 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

static int
	ft_strlen(const char *str)
{
	int	i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

int
	ft_isdigit(char *str)
{
	int	i;

	i = -1;
	while (str[++i])
	{
		if ((int)str[i] < 48 || (int)str[i] > 57)
			return (FALSE);
	}
	return (TRUE);
}

int
	ft_atoi(const char *str)
{
	int	i;
	int	result;

	i = 0;
	result = 0;
	while (*str == ' ' || *str == '\t' || *str == '\v' || *str == '\f'
		|| *str == '\r' || *str == '\n')
		++str;
	if (ft_strlen(str) > 10)
		return (-1);
	if (str[i] == '+' || str[i] == '-')
	{
		if (str[i] == '-')
			return (-1);
		i++;
	}
	while (str[i] >= '0' && str[i] <= '9')
	{
		result = result * 10 + str[i] - '0';
		i++;
		if (result > INT_MAX)
			return (-1);
	}
	return (result);
}
