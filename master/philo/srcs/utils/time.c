/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/10 04:36:33 by thzeribi          #+#    #+#             */
/*   Updated: 2022/04/01 23:17:43 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

long long
	get_current_time(void)
{
	struct timeval	tv;

	gettimeofday(&tv, NULL);
	return ((((long long) tv.tv_sec) * 1000) + (tv.tv_usec / 1000));
}

long long
	get_time_diff(long long old, long long current)
{
	return (current - old);
}

void
	ft_usleep(t_data *data, long long time)
{
	long long	old_time;

	old_time = get_current_time();
	while (!is_finish(data))
	{
		if (get_time_diff(old_time, get_current_time()) >= time)
			break ;
		usleep(1000);
	}
}
