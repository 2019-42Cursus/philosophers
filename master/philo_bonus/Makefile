# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/12/19 12:13:48 by thzeribi          #+#    #+#              #
#    Updated: 2022/04/03 21:23:02 by thzeribi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

################################################################################
#                                     CONFIG                                   #
################################################################################

ifndef VERBOSE
MAKEFLAGS += --no-print-directory --silent
endif
ifndef EMOJI
EMOJI=0
endif

DEFAULT_SPACE = 63

RULE = $(filter all re clean fclean doclean norm header test,$(MAKECMDGOALS))

AUTHOR	= thzeribi
DATE	= $(shell date +%F)

NAME				=	philo_bonus
PROJECT_NAME		=	Philosophers

################################################################################
#                                 SOURCES                                      #
################################################################################
SOURCES_FOLDER		=	./srcs/
INCLUDES_FOLDER		=	./privates/
OBJECTS_FOLDER		=	./objs/
TESTS_FOLDERS		=	./tests/

SOURCES := \
		philo.c \
		\
		threads/socrate.c \
		threads/supervisor.c \
		\
		actions/manager.c \
		actions/display.c \
		actions/display_utils.c \
		\
		utils/time.c \
		utils/parse_utils.c \
		utils/philo_utils.c \
		utils/exit.c \


################################################################################
#                                  OBJETS                                      #
################################################################################

OBJECTS		 =	$(SOURCES:.c=.o)
OBJECTS		:=	$(addprefix $(OBJECTS_FOLDER),$(OBJECTS))
SOURCES		:=	$(addprefix $(SOURCES_FOLDER),$(SOURCES))
DEPS		 =	$(OBJECTS:.o=.d)

################################################################################
#                                  FLAGS                                       #
################################################################################

FSANITIZE		=	-g3 -fsanitize=thread
#FLAGS			=	$(FSANITIZE)
#FLAGS			=	-MMD -Wall -Wextra -Werror -pthread -DEMOJI=${EMOJI} $(FSANITIZE) 
FLAGS			=	-MMD -Wall -Wextra -Werror -g -pthread -DEMOJI=${EMOJI}
CC				=	clang

INCLUDES		= -I$(INCLUDES_FOLDER)

################################################################################
#                                 LOGIC                                       #
################################################################################

NO_COLOR 		=	\033[38;5;15m
OK_COLOR		=	\033[38;5;2m
ERROR_COLOR		=	\033[38;5;1m
WARN_COLOR		=	\033[38;5;3m
SILENT_COLOR	=	\033[38;5;245m
INFO_COLOR		=	\033[38;5;140m
OBJ_COLOR		=	\033[0;36m

define print_header
	printf "%b" "$(INFO_COLOR)";
	echo "=============================================================================================="
	echo "|| $(OK_COLOR) _______  _______  _        _______  $(OBJ_COLOR)    _______ _________ _______  _______  _$(INFO_COLOR)           ||";
	echo "|| $(OK_COLOR)(       )(  ___  )| \    /\(  ____ \ $(OBJ_COLOR)   (  ___  )\__   __/(  ____ )(  ___  )( (    /|$(INFO_COLOR)    ||";
	echo "|| $(OK_COLOR)| () () || (   ) ||  \  / /| (    \/ $(OBJ_COLOR)   | (   ) |   ) (   | (    )|| (   ) ||  \  ( |$(INFO_COLOR)    ||";
	echo "|| $(OK_COLOR)| || || || (___) ||  (_/ / | (__  ___$(OBJ_COLOR)__ | (___) |   | |   | (____)|| |   | ||   \ | |$(INFO_COLOR)    ||";
	echo "|| $(OK_COLOR)| |(_)| ||  ___  ||   _ (  |  __)(___$(OBJ_COLOR)__)|  ___  |   | |   |     __)| |   | || (\ \) |$(INFO_COLOR)    ||";
	echo "|| $(OK_COLOR)| |   | || (   ) ||  ( \ \ | (       $(OBJ_COLOR)   | (   ) |   | |   | (\ (   | |   | || | \   |$(INFO_COLOR)    ||";
	echo "|| $(OK_COLOR)| )   ( || )   ( ||  /  \ \| (____/\ $(OBJ_COLOR)   | )   ( |   | |   |_)_\ \__| (___)_|| )_ \__| $(INFO_COLOR)   ||";
	echo "|| $(OK_COLOR)|/     \||/     \||_/    \/(_______/ $(OBJ_COLOR)   |/     \|   )_(    $(OK_COLOR)| _ )/ _ \| \| | | | / __|$(INFO_COLOR)    ||";
	echo "||                                                            $(OK_COLOR)| _ \ (_) | .\` | |_| \__ \    $(INFO_COLOR)||";
	printf "\033[m$(INFO_COLOR)|| $(OK_COLOR)\t\t\t\tName:	$(OBJ_COLOR)   $(PROJECT_NAME)       $(OK_COLOR)|___/\___/|_|\_|\___/|___/$(OBJ_COLOR).V2$(INFO_COLOR) ||\n"
	printf "$(INFO_COLOR)|| $(OK_COLOR)\t\t\t\tAuthor:	$(OBJ_COLOR)   $(AUTHOR)\033[m"
	$(call print_spaces,$(AUTHOR))
	printf "$(INFO_COLOR)|| $(OK_COLOR)\t\t\t\tDate: 	$(OBJ_COLOR)   $(DATE)\033[m"
	$(call print_spaces,$(DATE))
	printf "$(INFO_COLOR)|| $(OK_COLOR)\t\t\t\tCC: 	$(OBJ_COLOR)   $(CC)\033[m"
	$(call print_spaces,$(CC))
	printf "$(INFO_COLOR)|| $(OK_COLOR)\t\t\t\tFlags: 	$(OBJ_COLOR)   $(FLAGS)\033[m"
	$(call print_spaces,${FLAGS})
	printf "$(INFO_COLOR)|| $(OK_COLOR)\t\t\t\tRule: 	$(OBJ_COLOR)   $(RULE)\033[m"
	$(call print_spaces,$(RULE))
	echo "$(INFO_COLOR)||                                                                                          ||";
	echo "=============================================================================================="
endef

define print_spaces
	ARG="$(1)"; \
	LEN=$${#ARG}; \
	SIZE=$$(( $(DEFAULT_SPACE) - $${LEN} )); \
	printf "%$${SIZE}b" "$(INFO_COLOR)||\n";
endef

################################################################################
#                                 RULES                                        #
################################################################################

all: header $(NAME)

header:
	clear
	$(call print_header)

$(NAME): $(OBJECTS)
	printf "\t\t$(NO_COLOR)All objects for $(INFO_COLOR)$(PROJECT_NAME) $(NO_COLOR)where successfully created.\n"
	printf "\t\t$(INFO_COLOR)$(PROJECT_NAME) $(NO_COLOR)Removed all objects$(NO_COLOR).\n"
	printf "\n\t\t$(NO_COLOR)-------- $(INFO_COLOR) Start Compilation for ${PROJECT_NAME} $(NO_COLOR)--------\n"
	$(CC) $(FLAGS) $(INCLUDES) -o $(NAME) $(OBJECTS)
	printf "%-50s \r"
	printf "\t\t$(INFO_COLOR)$(NAME)$(NO_COLOR) successfully compiled. $(OK_COLOR)✓$(NO_COLOR)\n"

force: $(OBJECTS)
	printf "\t\t\t$(NO_COLOR)All objects for $(INFO_COLOR)$(PROJECT_NAME) $(NO_COLOR)where successfully created.\n"
	$(CC) $(FLAGS) $(INCLUDES) -o $(NAME) $(OBJECTS)
	printf "%-50s \r"
	printf "\t\t\t$(INFO_COLOR)$(NAME)$(NO_COLOR) successfully force compiled. $(OK_COLOR)✓$(NO_COLOR)\n"

-include $(DEPS)
$(OBJECTS_FOLDER)%.o: $(SOURCES_FOLDER)%.c
	mkdir -p $(@D)
	$(CC) $(FLAGS) $(INCLUDES) -c $< -o $@
	printf "%-50s \r"
	printf "\t\t\t$(NO_COLOR)Creating $(INFO_COLOR)%-30s $(OK_COLOR)✓$(NO_COLOR)\r" "$@"

clean: header
	rm -f $(OBJECTS)

fclean: clean
	rm -f $(NAME)
	rm -rf $(OBJECTS_FOLDER)
	printf "\t\t$(INFO_COLOR)$(PROJECT_NAME) $(NO_COLOR)Removed $(INFO_COLOR)$(NAME)$(NO_COLOR).\n"

norm: header
	printf "\n$(INFO_COLOR)$(PROJECT_NAME) $(NO_COLOR)Norm check$(NO_COLOR).\n\n"
	norminette ${SOURCES_FOLDER} ${INCLUDES_FOLDER} | grep 'Error' ; true

test:
	echo "Test is not invalable for this project !"

doclean: all clean

re: fclean all

coffee:
	bash /sgoinfre/goinfre/Perso/thzeribi/private/Scripts/coffee.sh

.PHONY: all re clean fclean force doclean norm coffee header test
