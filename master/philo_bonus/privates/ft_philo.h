/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_philo.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/03 05:54:06 by thzeribi          #+#    #+#             */
/*   Updated: 2022/04/06 06:41:17 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PHILO_H
# define FT_PHILO_H

/*
**		┌----------------┰-----------------------------------------------┐
**		|				 |												 |
**		|	SECTION		 |			INCLUDE AND DEFINE					 |
**		|				 |												 |
**		└----------------┸-----------------------------------------------┘
**
**
**		┌----------------------------------------------------------------┐
**		|						IMPORT PUBLIC LIB						 |
**		└----------------------------------------------------------------┘
*/

# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include <sys/time.h>
# include <semaphore.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/types.h>
# include <sys/wait.h>


/*
**		┌----------------------------------------------------------------┐
**		|						IMPORT PRIVATE LIB						 |
**		└----------------------------------------------------------------┘
*/

# include "struct.h"
# include "actions.h"
# include "color.h"

/*
**		┌----------------------------------------------------------------┐
**		|						DEFINE LOCAL VALUE						 |
**		└----------------------------------------------------------------┘
*/

# define TRUE 1
# define FALSE 0
# define INT_MAX 2147483647
# ifndef EMOJI
#  define EMOJI 0
# endif

/*
**		┌----------------┰-----------------------------------------------┐
**		|				 |												 |
**		|	SECTION		 |			FUNCTION PROTOTYPE					 |
**		|				 |												 |
**		└----------------┸-----------------------------------------------┘
**
**
**		┌----------------------------------------------------------------┐
**		|					INITIALISATION FUNCTION						 |
**		└----------------------------------------------------------------┘
*/

int			init_semaphores(t_data *data);
int			init_socrate(t_data *data);
int			create_socrate_process(t_data *data);

/*
**		┌----------------------------------------------------------------┐
**		|						GETTER FUNCTION							 |
**		└----------------------------------------------------------------┘
*/

int			is_finish(t_data *data);
int			get_sat_soc(t_data *data);

/*
**		┌----------------------------------------------------------------┐
**		|						SETTER FUNCTION							 |
**		└----------------------------------------------------------------┘
*/

void		set_is_finish(t_data *data, int boolean);
void		set_dug_grave(t_data *data, int boolean);
void		incr_satisfied_socrate(t_data *data);

/*
**		┌----------------------------------------------------------------┐
**		|						MANAGER FUNCTION						 |
**		└----------------------------------------------------------------┘
*/

void		grim_reaper(t_data *data);
void		action_manager(t_data *data, t_socrate *socrate, int id);
void		*supervisor(t_socrate *socrate);
void		take_forks(t_socrate *socrate, int id);

/*
**		┌----------------------------------------------------------------┐
**		|						DISPLAY FUNCTION						 |
**		└----------------------------------------------------------------┘
*/

void		display_visual(void);
void		display(t_data *data, char *action, int i);
void		display_time(long long start_time);
void		display_id(long long id);
void		display_end(t_data *data);
void		display_death(t_data *const data, int id);

/*
**		┌----------------------------------------------------------------┐
**		|							UTILS FUNCTION						 |
**		└----------------------------------------------------------------┘
*/

int			len_nb(long long nb);
void		print_space(int nb);
int			ft_atoi(const char *str);
int			ft_isdigit(char *str);
long long	get_current_time(void);
long long	get_time_diff(long long old, long long current);
void		ft_usleep(t_data *data, long long time);

/*
**		┌----------------------------------------------------------------┐
**		|							END FUNCTION						 |
**		└----------------------------------------------------------------┘
*/

void		exit_philo(t_data *data);

#endif
