/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/17 03:06:35 by thzeribi          #+#    #+#             */
/*   Updated: 2022/03/31 18:06:31 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void
	display_visual(void)
{
	printf("┌----------┰---------┰-----------------------┐\n");
	printf("| Time     | Socrate | Action                |\n");
	printf("|----------┸---------┸-----------------------|\n");
}

void
	display_end(t_data *data)
{
	display_time(data->start_time);
	if (EMOJI)
		printf("    All Socrates are satisfied   🙇 |\n%s",
			"└--------------------------------------------┘\n\n");
	else
		printf("All Socrates are satisfied\n");
}

void
	display_death(t_data *const data, int id)
{
	data->dug_grave = TRUE;
	display_time(data->start_time);
	display_id((long long)id);
	if (EMOJI)
		printf("is dead             💀 |\n%s",
			"└--------------------------------------------┘\n\n");
	else
		printf("died\n");
}

void
	display(t_data *data, char *action, int id)
{
	if (!data->is_finish)
	{
		display_time(data->start_time);
		display_id((long long)id);
		printf("%s", action);
	}
}
