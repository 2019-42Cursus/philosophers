/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manager.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/17 06:05:50 by thzeribi          #+#    #+#             */
/*   Updated: 2022/03/31 18:06:11 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"


void
	action_manager(t_data *data, t_socrate *socrate, int id)
{
	(void)data;
	(void)socrate;
	(void)id;
}
