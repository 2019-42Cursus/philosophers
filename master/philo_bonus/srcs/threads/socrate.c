/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   socrate.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/04/03 20:56:25 by thzeribi          #+#    #+#             */
/*   Updated: 2022/04/06 07:41:19 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

int
	init_semaphores(t_data *data)
{
	printf("INIT SEM\n");
	sem_unlink("/socrate_forks");
	sem_unlink("/socrate_desk");
	data->forks = sem_open("/socrate_forks", O_CREAT, S_IRWXU, data->philo_nbr);
	data->desk = sem_open("/socrate_desk", O_CREAT, S_IRWXU, 1);
	if (data->desk <= 0 || data->forks <= 0)
		return (FALSE);
	return (TRUE);
}

int
	init_socrate(t_data *data)
{
	int	i;

	i = -1;
	data->socrate = malloc(sizeof(t_socrate) * data->philo_nbr);
	if (data->socrate == NULL)
		return (FALSE);
	data->start_time = get_current_time();
	while (++i < data->philo_nbr)
	{
		data->socrate[i].id = i + 1;
		data->socrate[i].eat_count = 0;
		data->socrate[i].last_meal = data->start_time;
		data->socrate[i].data = data;
		data->socrate[i].is_eating = FALSE;
		data->socrate[i].l_fork = i;
		data->socrate[i].r_fork = (i + 1) % data->philo_nbr;
		data->socrate[i].philo_nbr = data->philo_nbr;
		data->socrate[i].time2eat = data->time2eat;
		data->socrate[i].time2die = data->time2die;
		data->socrate[i].time2sleep = data->time2sleep;
		data->socrate[i].must_eat = data->must_eat;
	}
	return (TRUE);
}

int
	create_socrate_process(t_data *data)
{
	int		i;

	i = -1;
	(void)data;
	while (++i < data->philo_nbr)
	{
		data->socrate[i].pid = fork();
		if (data->socrate[i].pid < 0)
			return (FALSE);
		else if (data->socrate[i].pid == 0)
		{
			supervisor(&data->socrate[i]);
			free(data->socrate);
			sem_close(data->forks);
			sem_close(data->desk);
			exit(1);
		}
		usleep(100);
	}
	// grim_reaper(data);
	return (TRUE);
}