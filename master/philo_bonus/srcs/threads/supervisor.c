/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   supervisor.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/19 13:26:28 by thzeribi          #+#    #+#             */
/*   Updated: 2022/04/06 06:40:56 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void
	*supervisor(t_socrate *socrate)
{
	t_data *const		data = socrate->data;
	int const			id = socrate->id;
	int const			philo_nbr = data->philo_nbr;

	// sem_wait(data->desk);
	// sem_post(data->desk);
	if (id % 2 == 0)
		usleep((philo_nbr * 50) + 100);
	printf("Socrate : %d\n", id);
	// while (!is_finish(data))
	// {
	// 	action_manager(data, socrate, id);
	// }
	return (NULL);
}
