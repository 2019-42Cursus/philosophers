/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: thzeribi <thzeribi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/15 02:46:49 by thzeribi          #+#    #+#             */
/*   Updated: 2022/04/06 07:39:10 by thzeribi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_philo.h"

void
	exit_philo(t_data *data)
{
	int	i;
	int	status;

	i = -1;
	while (++i < data->philo_nbr)
		waitpid(data->socrate[i].pid, &status, 0);
	free(data->socrate);
	sem_close(data->forks);
	sem_close(data->desk);
	exit(0);
}
